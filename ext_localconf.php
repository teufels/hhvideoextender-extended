<?php
defined('TYPO3') or die();

call_user_func(function() {
    $extensionKey = 'hhvideoextender_extended';

    // automatically add TypoScript, can be disabled in the extension configuration (BE)
    if (isset($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extensionKey]['config']['typoScript']) && $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extensionKey]['config']['typoScript'] === '1') {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('@import "EXT:'.$extensionKey.'/Configuration/TypoScript/constants.typoscript"');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup('@import "EXT:'.$extensionKey.'/Configuration/TypoScript/setup.typoscript"');
    }
    
    // Overwrite HhVideoExtender Classes
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['HauerHeinrich\\HhVideoExtender\\Rendering\\YouTubeRenderer'] = [
        'className' => 'Teufels\\HhvideoextenderExtended\\Rendering\\YouTubeRenderer'
    ];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['HauerHeinrich\\HhVideoExtender\\Rendering\\VimeoRenderer'] = [
        'className' => 'Teufels\\HhvideoextenderExtended\\Rendering\\VimeoRenderer'
    ];
});
