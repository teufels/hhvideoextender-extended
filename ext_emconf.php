<?php

/***************************************************************
 * Copyright notice
 *
 * (c) 2023 teufels GmbH <digital@teufels.com>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => '[teufels] Hauer-Heinrich - Video Extender Extended',
    'description' => 'Extend hauerheinrich/hh-video-extender EXT for Cookie Consent Tool Support for embedded youtube & vimeo content',
    'category' => 'plugin',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teudels.com',
    'author_company' => 'teufels GmbH',
    'state' => 'stable',
    'version' => '1.1.0',
    'clearCacheOnLoad' => 0,
    'constraints' => [
        'depends' => [
            'hh-video-extender' => '0.3.0-0.0.0',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
