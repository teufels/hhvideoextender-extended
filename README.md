[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-hhvideoextender--extended-orange.svg)](https://bitbucket.org/teufels/hhvideoextender-extended/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-hhvideoextender__extended-red.svg)](https://bitbucket.org/teufels/hhvideoextender-extended/src/main/)
![version](https://img.shields.io/badge/version-1.1.*-yellow.svg?style=flat-square)

[ ṯeufels ] Hauer-Heinrich - Video Extender Extended 
==========
Extend hauerheinrich/hh-video-extender EXT for Cookie Consent Tool Support for embedded youtube & vimeo content

### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/13_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req teufels/hhvideoextender-extended`

***

### Requirements
`hauerheinrich/hh-video-extender: ^0.3`

***

### How to use
- Install with composer
- (Optional) Import Static Template (before sitepackage)
  - will be automatically imported - on problems deactivate this in the EXT Settings and manually import
- in TS CONSTANTS set plugin.tx_hhvideoextender.cookieTool = <cookiebot || usercentrics>

***

### Migration beewilly to teufels
- replace `beewilly/hive_ovr_hhvideoextender` with `teufels/hhvideoextender-extended`

***

### Changelog
#### 1.1.0 
- [BREAKING] setting plugin.tx_hhvideoextender.settings.cookieTool has to moved from TypoScript/Setup to TypoScript/Constants
- [CHANGE] add support for TYPO3 v13
#### 1.0.0
- [Initial] intial from [hive_ovr_hhvideoextender](https://bitbucket.org/teufels/hive_ovr_hhvideoextender/src/)

***

### Conflict
- usage of TypoScript default previewImage may not work